package Oleg_Task7_Java_IO;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

public class UsersRepositoryFileImpl {
    public static void main(String[] args) throws IOException {
       // userfindById(3
        // delete(2);
       // updateUser(new User(2,"Nina","Rew",45,true));
 createUser(new User(6,"Frank","Revb",11,false));

    }

        public static void userfindById(int id) throws FileNotFoundException {
            String Id=Integer.toString(id);
            int findUserId=0;
            File file=new File("user");
            Scanner scan = new Scanner(file);
            while(scan.hasNext()){
                String line = scan.nextLine();
                String [] splitLine=line.split("\\|");
                for(int i=0;i<splitLine.length;i++){
                    if((splitLine[i].equals(Id))&&(i<3)){
                        findUserId=Integer.parseInt(splitLine[i]);
                        System.out.println(line);
                    } }
            }
            if(findUserId==0){
                System.out.println("user didn't find");
            }
            scan.close();
        }



        public static void createUser(User user)   {
            String inputFileName = "user";
            try(FileWriter writer = new FileWriter(inputFileName, true)){
            try (BufferedWriter bw = new BufferedWriter(writer)) {
                bw.write(user.toString() + " \n");
                System.out.println("Done");
            }}
            catch (IOException e){
                e.printStackTrace();
            }

        }
    public static void updateUser(User user)   {
        File sourceFile=new File("user");
        File outFile=new File("userCopy");

        HashMap<Integer,User> usersList=new HashMap<>();
        try (BufferedReader reader= new BufferedReader(new FileReader(sourceFile))){
        try (BufferedWriter writer=new BufferedWriter(new FileWriter(outFile))) {
            String line;
            while ((line = reader.readLine()) != null) {
                String[] parts = line.split("\\|");
                int ID = Integer.valueOf(parts[0]);
                String Name = parts[1];
                String lastName = parts[2];
                int age = Integer.parseInt(parts[3]);
                Boolean status = Boolean.parseBoolean(parts[4]);
                User user2 = new User(ID, Name, lastName, age, status);
                usersList.put(user2.getId(), user2);

            }

            usersList.put(user.getId(), user);
            System.out.println("User- " + usersList);

            for (Object user2 : usersList.values()) {
                String newLine = user2.toString();
                writer.write(newLine);
                writer.newLine();
            }

            reader.close();
            writer.close();
            sourceFile.delete();
            outFile.renameTo(sourceFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }


    public static void delete(int id)  {
        File sourceFile=new File("user");
        File outFile=new File("userCopy");
int DeleteNumber=id;
        HashMap<Integer,User> usersList=new HashMap<>();
       try (BufferedReader reader= new BufferedReader(new FileReader(sourceFile))){
      try( BufferedWriter writer=new BufferedWriter(new FileWriter(outFile))) {
          String line;
          while ((line = reader.readLine()) != null) {
              String[] parts = line.split("\\|");
              int ID = Integer.valueOf(parts[0]);
              String Name = parts[1];
              String lastName = parts[2];
              int age = Integer.parseInt(parts[3]);
              Boolean status = Boolean.parseBoolean(parts[4]);
              User user = new User(ID, Name, lastName, age, status);
              usersList.put(user.getId(), user);

          }

          usersList.remove(DeleteNumber);
          System.out.println("User- " + usersList);

          for (Object user : usersList.values()) {
              String newLine = user.toString();
              writer.write(newLine);
              writer.newLine();
          }

          reader.close();
          writer.close();
          sourceFile.delete();
          outFile.renameTo(sourceFile);
      }}
       catch(IOException e){
           e.printStackTrace();
       }
    }
}




