package Oleg_Task7_Java_IO;

public class User {

        public User (int id, String name, String surname, int age, boolean haveGotJob) {
            this.id = id;
            this.name = name;
            Surname = surname;
            this.age = age;
            this.haveGotJob = haveGotJob;
        }

        int id;
        String name;
        String Surname;
        int age;
        boolean haveGotJob;

        public void setId(int id) {
            this.id = id;
        }

        public void setName(String name) {
            this.name = name;
        }

        public void setSurname(String surname) {
            Surname = surname;
        }

        public void setAge(int age) {
            this.age = age;
        }

        public void setHaveGotJob(boolean haveGotJob) {
            this.haveGotJob = haveGotJob;
        }

        public int getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        public String getSurname() {
            return Surname;
        }

        public int getAge() {
            return age;
        }

        public boolean isHaveGotJob() {
            return haveGotJob;
        }
        @Override
        public String toString(){
            return id+"|"+name+"|"+Surname+"|"+age+"|"+haveGotJob;
        }
    }

