package OLeg_Task5_31_08_22;

import org.junit.jupiter.api.Test;

import java.lang.reflect.Array;

import static org.junit.jupiter.api.Assertions.*;

class MainTest {
@Test
    void test1(){
    int [] ArrayCheck={1,1,2,4};

    assert Calc.Addition(ArrayCheck)==8;
}
    @Test
    void test2(){
        int [] ArrayCheck={1,1,2,4};

        assert Calc.Deduction(ArrayCheck)==0;
    }
@Test
void test3(){
    int [] ArrayCheck={1,10,2,400};
    assert Calc.Division(ArrayCheck)==2;
}
    @Test
    void test4(){
        int [] ArrayCheck={1,1,2,4};
        assert Calc.Multiplication(ArrayCheck)==8;
    }
    @Test
    void test5(){

        assert Calc.Factorial(5)==120;
    }
    @Test
    void test6(){
        int [] ArrayCheck={100,10,22,4};
        assert Calc.Division(ArrayCheck)==5;
    }
    @Test
    void test7(){
        int [] ArrayCheck={11,111,1,1111};
        assert Calc.Division(ArrayCheck)==111;
    }
    @Test
    void test8(){
        int [] ArrayCheck={110,11,1,4};
        assert Calc.Division(ArrayCheck)==2;
    }
    @Test
    void test9(){
        int [] ArrayCheck={5,11,31,7};
        assert Calc.Division(ArrayCheck)==4;
    }
    @Test
    void test10(){
        int [] ArrayCheck={15,41,168,7};
        assert Calc.Division(ArrayCheck)==24;
    }
}