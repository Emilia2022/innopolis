package OLeg_Task4_17_08_22;

public class Ellipse extends Figure {

int a,b;//Полуоси

    public Ellipse(int x, int y, int a, int b) {
        super(x, y);
        this.a = a;
        this.b = b;
    }

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    public int getB() {
        return b;
    }

    public void setB(int b) {
        this.b = b;
    }

    @Override
    public double getPerimeter() {

        return (double)  (2*Math.PI*Math.sqrt((Math.pow(getA(),2)+Math.pow(getB(),2))/2));

    }
}
