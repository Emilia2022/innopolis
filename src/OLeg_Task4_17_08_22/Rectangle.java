package OLeg_Task4_17_08_22;

public class Rectangle extends Figure{

   int a,b;

    public Rectangle(int x, int y, int a, int b) {
        super(x, y);
        this.a = a;
        this.b = b;
    }

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    public int getB() {
        return b;
    }

    public void setB(int b) {
        this.b = b;
    }

    @Override
    public double getPerimeter() {
        return (double) 2*(getA()+getB());
    }
}
