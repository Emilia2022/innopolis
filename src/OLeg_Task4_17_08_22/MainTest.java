package OLeg_Task4_17_08_22;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MainTest {
    @Test
    void test_getPerimeter_Circle(){
        Circle circle=new Circle(1,1,4);
        double p1=circle.getPerimeter();
        circle.move(1,1);
        double p2=circle.getPerimeter();
        assert p1==p2;

    }
    @Test
    void test_getPerimeter_Square(){
        Square square=new Square(1,1,2);
        double p1=square.getPerimeter();
        square.move(1,1);
        double p2=square.getPerimeter();
        assert p1==p2;

    }


}