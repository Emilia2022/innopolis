package OLeg_Task4_17_08_22;

public class Square extends Rectangle implements Moveable {

    public Square(int x, int y, int a) {
        super(x, y, a, a);
    }

    @Override
    public double getPerimeter() {
      return (double) 4*getA();
    }

    @Override
    public void move(int x,int y) {
        this.x=getX();
        this.y=getY();
        System.out.println("x= "+getX()+" y= "+getY());
    }

}
