package OLeg_Task4_17_08_22;

import OLeg_Task4_17_08_22.*;
//Сделать абстрактный класс Figure, у данного класса есть два поля - x и y координаты.
//        Классы Ellipse и Rectangle должны быть потомками класса Figure.
//        Класс Square - потомок класса Rectangle, Circle - потомок класса Ellipse.
//        В классе Figure предусмотреть абстрактный метод getPerimeter().
//        Так же, нужно определить интерфейс Moveable c единственным методом .move(int x, int y), который позволит перемещать фигуру на заданные координаты.
//        Данный интерфейс должны реализовать только классы Circle и Square.
//        В Main создать массив всех фигур и "перемещаемых" фигур. У всех вывести в консоль периметр, а у "перемещаемых" фигур изменить случайным образом координаты.

public class Main {

    public static void main(String[] args) {
        Figure[] perimeter =new Figure[4];
        perimeter[0]=new Ellipse(1,1,2,2);
        perimeter[1]=new Rectangle(1,1,1,3);
        perimeter[2]=new Circle(1,1,4);
        perimeter[3]=new Square(1,1,2);

        for (int i=0;i<perimeter.length;i++){
            System.out.println("Perimeter = "+perimeter[i].getPerimeter());

        }

        Circle circle=new Circle(1,1,4);
        Square square=new Square(1,1,2);
//убрать рандом из класса квадрат и круг
        circle.x= (int) (circle.x+(Math.random()*10));
        circle.y= (int) (circle.y+Math.random()*10);
        circle.move(1,1);

        square.x= (int) (square.x+(Math.random()*10));
        square.y= (int) (square.y+Math.random()*10);
        square.move(1,1);

        System.out.println("circle perimeter = "+circle.getPerimeter());
        System.out.println("square perimeter = "+square.getPerimeter());






    }
}
