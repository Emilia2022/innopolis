package OLeg_Task4_17_08_22;
//abstract class can't allow to create objects
public abstract class Figure {
 int x,y;

    public Figure(int x, int y) {
        this.x = x;
        this.y = y;
    }
    public abstract double getPerimeter();

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
}
