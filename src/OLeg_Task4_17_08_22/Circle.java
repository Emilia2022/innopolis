package OLeg_Task4_17_08_22;

public class Circle extends Ellipse implements Moveable{
    //периметр равен PiR; r-это одна их осей. У круга оси равны a=b;


    public Circle(int x, int y, int a) {
        super(x, y, a,a);
    }

    @Override
    public double getPerimeter() {

      return (double) (getA()*Math.PI);
    }

    @Override
    public void move(int x,int y) {
     this.x=getX();
     this.y=getY();
        System.out.println("x= "+getX()+" y= "+getY());
    }
}

