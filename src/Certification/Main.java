package Certification;

import java.io.*;
import java.util.HashMap;

public class Main {
    public static void main(String[] args) {
        
        File file = new File("book");
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            try( BufferedWriter writer=new BufferedWriter(new FileWriter("result.txt",true))){
                String line=null;

                HashMap<String, Integer> wordToCount = new HashMap<>();
                while ((line=reader.readLine())!=null) {

                    String cleanString = preProcess(line);
                    String[] lines = cleanString.split(" ");
                    for (String word : lines) {
                        if (!wordToCount.containsKey(word)) {
                            wordToCount.put(word, 0);
                        }
                        wordToCount.put(word, wordToCount.get(word) + 1);
                    }
                    for (String word : wordToCount.keySet()) {
                        System.out.println(word + " " + wordToCount.get(word));
                        writer.write(word+" - "+wordToCount.get(word));
                        writer.newLine();
                    }

                }
                reader.close();
                writer.close();

            }

            catch (IOException e){
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    public static String preProcess(String line) {

        StringBuilder builder = new StringBuilder();
        char[] k = line.toCharArray();
                for (int i = 0; i < k.length; ++i) {
                    char t = k[i];
                    if (t == '.' || t == ';' || t == '?' || t == ',' || t == '"' || t == '!') {
                        continue;
                    }

                    if (t == ' ' && i < k.length - 1 && k[i + 1] == ' ') {
                        continue;
                    }

                    builder.append(t);

                }

        return builder.toString();
    }

    }


