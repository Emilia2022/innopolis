package Denis_calendars_18_08_22;

public class Gregorian extends Calendars implements Date{

    public Gregorian(int day, int month, int year) {
        super(day, month, year);
    }

    @Override
    public String addDay() {
        this.day=this.day+1;
        if (this.day>31){
            this.day=1;
            this.month=this.month+1;

        }
        if (this.month>12){
            this.month=1;
            this.year=this.year+1;
        }
        return (Integer.toString(getDay())+"."+Integer.toString(getMonth())+"."+Integer.toString(getYear()));
    }

    @Override
    public String detectDisplayName() {
        return (Integer.toString(getDay())+"."+Integer.toString(getMonth())+"."+Integer.toString(getYear()));
    }


}
