package Denis_calendars_18_08_22;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MainTest {
    @Test
    void test_1(){
        Gregorian gregorian= new Gregorian(11,11,2011);
        assert gregorian.detectDisplayName().equals("11.11.2011");
    }
    @Test
    void test_2(){
        Gregorian gregorian= new Gregorian(12,11,2011);
        assert gregorian.addDay().equals("13.11.2011");
    }
    @Test
    void test_3(){
        Islamic islamic = new Islamic(1,12,2000);
        assert islamic.detectDisplayName().equals("1.12.2000");
    }
    @Test
    void test_4(){
        Islamic islamic = new Islamic(31,12,2011);
       assert islamic.addDay().equals("1.1.2012");
    }

}