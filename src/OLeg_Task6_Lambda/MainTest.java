package OLeg_Task6_Lambda;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

class MainTest {
    @Test
    void test_1(){
        int [] arrayTest={1,22,1212,4};
        ByCondition byCondition=(number)->(number%2)==0;

        Assertions.assertEquals(
                Arrays.toString(Sequence.filter(arrayTest,byCondition)),
                Arrays.toString(new int[] { 22, 1212, 4 }));

    }
    @Test
    void test_2(){
        int [] arrayTest={1,22,1212,46,4,121};
        ByCondition byConditionSumDigits=(number) -> {
            int sum=0;
            while(number>0){
                sum = sum+ number % 10;
                number = number / 10;
            }
            return sum%2==0;
        };

        Assertions.assertEquals(
                Arrays.toString(Sequence.filter(arrayTest,byConditionSumDigits)),
                Arrays.toString(new int[] {22, 1212 ,46,4,121}));

    }
    @Test
    void test_3(){
        int [] arrayTest={122,24,11,2222,321,1101};
        ByCondition byConditionAllDigitsEven=(number) ->{

            int element = 0;
            if (number>0){
                element=number%10;

            }
            return element%2==0;
        };

        Assertions.assertEquals(
                Arrays.toString(Sequence.filter(arrayTest,byConditionAllDigitsEven)),
                Arrays.toString(new int[] {122, 24 ,2222}));

    }
    @Test
    void test_4(){
        int [] arrayTest={122,24,11,2222,321,1111,101};
        ByCondition byConditionPalindrome = (number) -> Integer.toString(number)
                .equals(new StringBuilder(Integer.toString(number))
                        .reverse().toString());

        Assertions.assertEquals(
                Arrays.toString(Sequence.filter(arrayTest,byConditionPalindrome)),
                Arrays.toString(new int[] {11, 2222 ,1111,101}));

    }
}