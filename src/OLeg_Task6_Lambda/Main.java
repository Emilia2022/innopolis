package OLeg_Task6_Lambda;

import com.sun.source.doctree.SummaryTree;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.stream.IntStream;
//В main в качестве condition подставить:
//+1.проверку на четность элемента
//+2.проверку, является ли сумма цифр элемента четным числом.
//+3.Доп. задача: проверка на четность всех цифр числа. (число состоит из цифр)
//+4.Доп. задача: проверка на палиндромность числа (число читается одинаково и слева, и справа -> 11 - палиндром, 12 - нет, 3333 - палиндром, 3211 - нет, и т.д.).

public class Main {

        public static void main(String[] args) {
            int [] array={2,9,8,4111,1,11,12211,21,22,444,42};

            ByCondition byCondition=(number)->(number%2)==0;
            System.out.println("parity check elements = "+ Arrays.toString(Sequence.filter(array,byCondition)));

            ByCondition byConditionSumDigits=(number) -> {
                int sum=0;
                while(number>0){
                     sum = sum+ number % 10;
                    number = number / 10;
                }
                return sum%2==0;
            };
            System.out.println("sum digits array is even= "+Arrays.toString(Sequence.filter(array,byConditionSumDigits)));

            ByCondition byConditionAllDigitsEven=(number) ->{

                int element = 0;
                if (number>0){
                    element=number%10;

                }
return element%2==0;
            };
            System.out.println("all digits array is even= "+Arrays.toString(Sequence.filter(array,byConditionAllDigitsEven)));

            ByCondition byConditionPalindrome = (number) -> Integer.toString(number)
                    .equals(new StringBuilder(Integer.toString(number))
                            .reverse().toString());
            System.out.println("palindromes="+Arrays.toString(Sequence.filter(array,byConditionPalindrome)));
        }

    }
    class Sequence {
        public static int[] filter(int[] array, ByCondition byCondition) {
            int count = 0;
            for (int i = 0; i < array.length; ++i) {//count size new array
                if (byCondition.isOk(array[i])) {
                    count++;
                }
            }
            int index = 0;
            int[] arrayNew = new int[count];//create new array
            for (int i = 0; i < array.length; ++i) {
                if (byCondition.isOk(array[i])) {
                    arrayNew[index] = array[i];
                    index++;
                }
            }


            return arrayNew;
        }

}
