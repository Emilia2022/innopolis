package Certification2;

import java.io.*;
import java.util.HashMap;

public class Main {
    public static void main(String[] args) {
        File file = new File("letter");
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
try(BufferedWriter writer=new BufferedWriter(new FileWriter("lettersResult"))) {
    HashMap<Character, Integer> lettersToCount = new HashMap<>();
    String line=null;
    while ((line=reader.readLine())!=null){
        String lineLower=line.toLowerCase();
       char[] strToArray=lineLower.toCharArray();

        for (char letter : strToArray) {
            if (!lettersToCount.containsKey(letter)) {
                lettersToCount.put(letter, 0);
            }
            if (letter == '@' || letter == ' ' ||letter == '&' ||letter == '"' || letter == '?' || letter == ',' || letter == '"' || letter == '!') {
                continue;
            }
            lettersToCount.put(letter, lettersToCount.get(letter) + 1);
        }
        for (char letter : lettersToCount.keySet()) {
            if(lettersToCount.get(letter)>0){
                System.out.println(letter + "- " + lettersToCount.get(letter));
                writer.write(letter+" - "+lettersToCount.get(letter));
                writer.newLine();
            }


        }



    }
}

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}